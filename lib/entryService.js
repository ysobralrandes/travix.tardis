'use strict'

module.exports = function() {

    const service = require('../lib/db/dbService')();

    return {
        addEntries: _addEntries,
        getEntries: _getEntries,
    };

    function _addEntries(entries) {
        return new Promise((resolve, reject) => {

            entries.every(entry => entry.__createdOn = new Date())

            service
                .addEntries(entries)
                .then(resolve)
                .catch(reject);
        });
    }
    
    function _getEntries(filter) {
        return new Promise((resolve, reject) => {
            return service
                .getEntries(filter)
                .then(resolve)
                .catch(reject);
        });
    }

    function _deleteEntries(filter) {
        return new Promise((resolve, reject) => {
            return service
                .getEntries(filter)
                .then(resolve)
                .catch(reject);
        });
    }
}