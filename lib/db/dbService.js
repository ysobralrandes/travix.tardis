'use strict'

module.exports = function() {

    const BigQuery = require('@google-cloud/bigquery');

    const projectId = "travix-bi-test";
    const datasetId = "tardis";
    const tableId = "JiraStories";

    const bigQuery = new BigQuery({
        projectId: projectId,
        keyFilename: "./security/tardis-travix-bi.json"
    });

    return {
        addEntries: _addEntries,
        getEntries: _getEntries
    };

    function _addEntries(entries) {
        return new Promise((resolve, reject) => {

            bigQuery
                .dataset(datasetId)
                .table(tableId)
                .insert(entries)
                .then(resolve)
                .catch(_onAddEntriesError);

            function _onAddEntriesError(err) {
                if (err && err.name === 'PartialFailureError') {
                    if (err.errors && err.errors.length > 0) {
                        console.log('Insert errors:');
                        err.errors.forEach(err => console.error(err));
                    }
                } else {
                    console.error('ERROR:', err);
                }

                reject(err);
            }
        });
    }
    
    function _getEntries(filter) {
        return new Promise((resolve, reject) => {

            const sqlQuery = `WITH lastEntries AS
                                (
                                SELECT key, MAX(__createdOn) AS __createdOn,     
                                    reporter.email,
                                    reporter.date
                                    FROM ${datasetId}.${tableId}, UNNEST(timespent) timespent
                                    WHERE timespent.amount > 0
                                    AND reporter.email = @userName
                                    AND reporter.date BETWEEN @initialDate AND @finalDate
                                GROUP BY key, reporter.email, reporter.date 
                                )
                                
                                
                                SELECT 
                                    entry.*
                                FROM ${datasetId}.${tableId} AS entry, lastEntries
                                WHERE entry.key = lastEntries.key
                                AND entry.__createdOn = lastEntries.__createdOn
                                AND entry.reporter.email = lastEntries.email
                                AND entry.reporter.date = lastEntries.date
                                AND reporter.email = @userName
                                AND reporter.date BETWEEN @initialDate AND @finalDate`;
            
            // Query options list: https://cloud.google.com/bigquery/docs/reference/v2/jobs/query
            const options = {
                query: sqlQuery,
                timeoutMs: 10000, // Time out after 10 seconds.
                useLegacySql: false, // Use standard SQL syntax for queries.
                parameterMode: "NAMED",
                queryParameters: [
                    _getParameterFor("STRING", "username", filter.userName), 
                    _getParameterFor("TIMESTAMP", "initialDate", filter.initialDate), 
                    _getParameterFor("TIMESTAMP", "finalDate", filter.finalDate)
                ]
            };

            // Runs the query
            bigQuery
                .query(options)
                .then((data) => resolve(data[0]))
                .catch(err => {
                    console.error('ERROR:', err);
                    reject(err);
                });

            function _getParameterFor(type, name, value) {
                return {
                    "parameterType": {
                      "type": type
                    },
                    "parameterValue": {
                      "value": value
                    },
                    "name": name
                  };
            }
        });
    }
}