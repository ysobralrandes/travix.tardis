'use strict'

module.exports = function() {

    const service = require('../lib/entryService')();

    return {
        addEntries: _addEntries,
        getEntries: _getEntries
    };

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    function _addEntries(req, res, next) {
        service
            .addEntries(req.body.entries)
            .then(() => {
                console.log('called entryService.addEntries');
                res.send(200);
                next();
            })
            .catch((error) => {
                console.log(`Error occurred while trying to add entries: ${error.message}`);
                res.send(500, { error: `Error occurred while adding entry: ${error.message}`});
                next();
            });
    }

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    function _getEntries(req, res, next) {
        service
            .getEntries({userName: req.query.email, initialDate: req.query.initialDate, finalDate: req.query.finalDate})
            .then((entries) => {
                console.log('called entryService.getEntries');
                res.send(200, entries);
                next();
            })
            .catch((error) => {
                console.log(`Error occurred while querying: ${error.message}`);
                res.send(500, { error: `Error occurred while querying: ${error.message}`});
                next();
            });
    }
}