'use strict'

/**
 * Module Dependencies
 */
const config  = require('./config'),
      restify = require('restify'),
      corsMiddleware = require('restify-cors-middleware');

/**
 * Initialize Server
 */
const server = restify.createServer({
    name    : config.name,
    version : config.version
})

/**
 * Middlewares
 */
server.use(restify.plugins.jsonBodyParser({ mapParams: true }))
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())

/**
 * Start server
 */
server.listen(config.port, () => {

    console.log(`Server listening on port ${config.port}`);

    var db = {};
    
    require('./routes')(server);

})