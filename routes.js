'use strict'

module.exports = function(server) {

    const entryController = require('./controllers/entryController')();

    server.post('/entries', entryController.addEntries);
    server.get('/entries', entryController.getEntries);
    // server.put('/entries/:id', entryController.updateEntries);
    // server.del('/entries/:id', entryController.removeEntries);
}